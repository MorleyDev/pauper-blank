##############################################
# Enable required C++14 flags on compilation #
##############################################

option(FORCE_CLANG_CXX14_FLAG "Force the clang c++14 flag to always be enabled" OFF)
option(FORCE_GNU_CXX14_FLAG "Force the gnu c++14 flag to always be enabled" OFF)
option(FORCE_GNU_CXX1Y_FLAG "Force the gnu c++1Y flag to always be enabled" OFF)

if(NOT GNU_CXX14_FLAG)
	set(GNU_CXX14_FLAG "-std=c++14 -pthread -Wl,--no-as-needed")
endif()
if(NOT GNU_CXX1Y_FLAG)
	set(GNU_CXX1Y_FLAG "-std=c++1y -pthread")
endif()
if(NOT CLANG_CXX14_FLAG)
	set(CLANG_CXX14_FLAG "-std=c++14")
endif()

if(FORCE_GNU_CXX1Y_FLAG)
	set(CMAKE_CXX_FLAGS "${CLANG_CXX14_FLAG} ${CMAKE_CXX_FLAGS}")
elseif(FORCE_GNU_CXX14_FLAG)
	set(CMAKE_CXX_FLAGS "${GNU_CXX14_FLAG} ${CMAKE_CXX_FLAGS}")
elseif(FORCE_GNU_CXX1Y_FLAG)
	set(CMAKE_CXX_FLAGS "${GNU_CXX1Y_FLAG} ${CMAKE_CXX_FLAGS}")
elseif(MSVC)
	set(CMAKE_CXX_FLAGS "/MP ${CMAKE_CXX_FLAGS}")
elseif(CMAKE_COMPILER_IS_GNUCXX)
	execute_process(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GXX_VERSION)
	if (GXX_VERSION VERSION_GREATER 4.9)
		set(CMAKE_CXX_FLAGS "${GNU_CXX14_FLAG} ${CMAKE_CXX_FLAGS}")
	else()
		set(CMAKE_CXX_FLAGS "${GNU_CXX1Y_FLAG} ${CMAKE_CXX_FLAGS}")
	endif()
elseif ("${CMAKE_CXX_COMPILER} ${CMAKE_CXX_COMPILER_ARG1}" MATCHES ".*clang")
	set(CMAKE_CXX_FLAGS "${CLANG_CXX14_FLAG} ${CMAKE_CXX_FLAGS}")
endif()

