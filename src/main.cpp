#include "MainGame.hpp"
#include <ppr/run.hpp>

int main(int nArgs, const char** ppcArgs) {

	try {
		app::MainGame game;
		ppr::run(game);
	} catch(std::exception const& e) {
		std::cerr << e.what() << std::endl;
		return 1;
	} catch(...) {
		std::cerr << "Unknown error occurred" << std::endl;
		return 2;
	}
	return 0;
}
