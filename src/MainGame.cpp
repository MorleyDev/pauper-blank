#include "MainGame.hpp"

#include <ppr/data/Window.hpp>
#include <ppr/events/Close.hpp>

app::MainGame::~MainGame() {
}

app::MainGame::MainGame()
		: Game("{GAME TITLE GOES HERE}") {
	window.onClose([this]() { events.enqueue(ppr::events::Close()); });
}
