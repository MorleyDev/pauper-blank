#ifndef APP_MAINGAME_HPP_INCLUDED
#define APP_MAINGAME_HPP_INCLUDED

#include <ppr/Game.hpp>

namespace app {
    class MainGame : public ppr::Game {
    private:
    public:
	    virtual ~MainGame();

	    MainGame();
    };

}

#endif//APP_MAINGAME_HPP_INCLUDED

