include(files.cmake)

add_empty_target(data)
add_dependencies(resources data)

function(add_data)
	foreach(NAME ${ARGN})
		install(FILES ${CMAKE_CURRENT_LIST_DIR}/${NAME} DESTINATION resources/data)
	endforeach(NAME)
endfunction(add_data)

add_data(${RESOURCES_DATA_FILES})
